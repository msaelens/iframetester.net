﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Content.Web.Startup))]
namespace Content.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
