﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Content.Web.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			return View();
		}

		public ActionResult About()
		{
			// Put down a cookie.
			var cookie = new HttpCookie("Cookie")
							{
								Value =
									"Here's your cookie. CreatedOn: "
									+ DateTime.Now.ToLongTimeString()
							};
			ControllerContext.HttpContext.Response.Cookies.Add(cookie);

			return View();
		}
		public ActionResult Contact()
		{
			ViewBag.Message = "Your contact page.";

			return View();
		}
	}
}